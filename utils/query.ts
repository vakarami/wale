import { FilterQuery } from "mongoose";
import { AdType } from "../models/ad";

export type ReqQuery = {
  price: string;
  date: string;
  search: string;
  [index: string]: string;
  limit: string;
  skip: string;
  sort: string; 
}
export type QueryOptions={
    limit: number,
    skip: number,
    sort: {
      [index: string]: number //sort by date desc
    },
}
export function extractQuery(reqQuery: ReqQuery) : FilterQuery<AdType> {
  const query: FilterQuery<AdType> = {};
  if (reqQuery.price) {
    const parts = reqQuery.price.split("-");
    query.price = { $gte: parseInt(parts[0]) };
    if (parts[1]) {
      query.price.$lt = parseInt(parts[1]);
    }
  }
  if (reqQuery.date) {
    const parts = reqQuery.date.split("-");
    const date = new Date();
    const interval = parseInt(parts[0]);
    switch (parts[1]) {
      case "h":
        date.setHours(date.getHours() - interval);
        break;
      case "d":
        date.setDate(date.getDate() - interval);
        break;
    }
    query.createdAt = {$gte: date}
  }
  const adFields = ["category", "owner", "currency", "city"];
  for (const field of adFields) {
    if (reqQuery[field]) {
      query[field] = reqQuery[field];
    }
  }
  if (reqQuery.search) {
    query.$text = { $search: reqQuery.search };
  }
  return query;
}

export function extractOptions(reqQuery: ReqQuery): QueryOptions {
  const options = {
    limit: 20,
    skip: 0,
    sort: {
      createdAt: -1, //sort by date desc
    },
  } as QueryOptions;
  if (reqQuery.limit) {
    options.limit = Math.min(30, parseInt(reqQuery.limit));
  }
  if (reqQuery.skip) {
    options.skip = parseInt(reqQuery.skip);
  }
  if (reqQuery.sort) {
    const parts = reqQuery.sort.split(":");
    const sort = {} as {[i:string]: number};
    sort[parts[0]] = parts[1] === "desc" ? -1 : 1;
    options.sort = sort;
  }
  return options;
}

