import jwt from "jsonwebtoken";
import type { RequestHandler, Request, Response, NextFunction } from "express";
import User, { UserType } from "../models/user";

export interface UserRequest extends Request{
  user?: UserType;
  token?: string;
}

export const auth = async (req: UserRequest, res: Response, next: NextFunction) => {
  try {
    const { user, token } = await findUser(req);

    if (!user) {
      throw new Error();
    }

    req.token = token;
    req.user = user;
    next();
  } catch (e) {
    res.status(401).send({ error: "Please login first" });
  }
};

export const noAuth: RequestHandler = async (req, res, next) => {
  try {
    const { user } = await findUser(req);

    if (user) {
      res.status(403).send({ error: "Operation not valid, you're already logged in" });
      return;
    }

    next();
  } catch (e) {
    next();
  }
};

async function findUser(req: Request): Promise<{user?: UserType | null, token?: string}> {
  const token = req.header("Authorization")?.replace("Bearer ", "");
  if(!token){
    return {};
  }
  const decoded = jwt.verify(token, process.env.JWT_SECRET as string) as {_id:string, time: Date};
  const user = await User.findOne({
    _id: decoded._id,
    "tokens.token": token,
  });
  return { user, token };
}

