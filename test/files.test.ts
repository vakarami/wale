import request from "supertest";
import mongoose from "mongoose";
import fs from "fs";
import app from "../app";
import User from "../models/user";

const ownderId = new mongoose.Types.ObjectId();
const testUser = {
  name: "test user",
  email: "user112@walle.com",
  password: "pas345sdf",
  _id: ownderId,
};
beforeAll(async () => {
  await User.deleteMany();
  const user = new User(testUser);
  await user.generateAuthToken();
});

describe("File Upload API", () => {
  it("should post new photos", async () => {
    const user = await User.findById(ownderId);
    expect(user).not.toBeNull();
    if(!user || !user.tokens){
      return
    }
    const res = await request(app)
      .post("/photos/upload")
      .set("Authorization", "Bearer " + user.tokens[0].token)
      .attach("photos", "./test/resources/img1.jpg")
      .attach("photos", "./test/resources/img2.jpg")
      .expect(200);
    expect(res.body).toHaveLength(2);

    await new Promise((r) => setTimeout(r, 200));//saving files takes time in server side

    for (const file of res.body) {
      expect(fs.existsSync("./" + file)).toBeTruthy();
      await request(app)
        .get("/" + file)
        .expect(200);
      fs.unlinkSync("./" + file);
    }
  });
});
