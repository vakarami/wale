import request from "supertest";
import mongoose from "mongoose";
import app from "../app";
import Ad from "../models/ad";
import User from "../models/user";

const adId = new mongoose.Types.ObjectId();
const ownderId = new mongoose.Types.ObjectId();
const testUser = {
  name: "test user",
  email: "user112@walle.com",
  password: "pas345sdf",
  _id: ownderId,
};
beforeAll(async () => {
  await Ad.deleteMany();
  await User.deleteMany();
  const user = new User(testUser);
  await user.generateAuthToken();
});

describe("Task Basi API", () => {
  it("should post new ads", async () => {
    const user = await User.findById(ownderId);
    expect(user).not.toBeNull();
    if(!user || !user.tokens){
      return
    }
    await request(app)
      .post("/ads/new")
      .set("Authorization", "Bearer " + user?.tokens[0].token)
      .send({
        _id: adId,
        title: "I'm selling my ca",
        description: "it's an old car",
        price: 122,
        category: "car",
        photos: ["photos/date/1.webp", "/photos/date/2.webp"],
      })
      .expect(201);
  });

  it("should get ads list", async () => {
    const res = await request(app).get("/ads");

    expect(res.status).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
  });

  it("should get ads list by filter", async () => {
    const res = await request(app).get("/ads?price=120-130&category=car");

    expect(res.status).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
    expect(res.body[0].photos).toHaveLength(2);
  });

  it("should get ads list by skip and limit", async () => {
    const res = await request(app).get("/ads?limit=1&skip=0&search=car");

    expect(res.status).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
  });

  it("should get 0 ads list by filter", async () => {
    const res = await request(app).get("/ads?price=120-130&category=home");

    expect(res.status).toBe(200);
    expect(res.body.length).toBe(0);
  });

  it("should get 0 ads list by skip and limit", async () => {
    const res = await request(app).get("/ads?limit=1&skip=1");

    expect(res.status).toBe(200);
    expect(res.body.length).toBe(0);
  });

  it("should get ad by id", async () => {
    const res = await request(app).get("/ad/" + adId);

    expect(res.status).toBe(200);
    expect(res.body._id).toBe(adId.toString());
  });

  it("should get no ad by id", async () => {
    const res = await request(app).get("/ad/60818799be3c2a1e7beaeca8");

    expect(res.status).toBe(404);
  });

  it("should update existing ad", async () => {
    const user = await User.findById(ownderId);
    const updatedAd = {
      title: "I'm selling my home",
      description: "it's an old home",
      category: "home",
    };
    expect(user).not.toBeNull();
    if(!user || !user.tokens){
      return
    }
    await request(app)
      .patch("/ad/" + adId)
      .set("Authorization", "Bearer " + user.tokens[0].token)
      .send(updatedAd)
      .expect(200);
    const ad = await Ad.findById(adId);
    expect(ad).not.toBeNull();
    expect(ad?.title).toBe(updatedAd.title);
    expect(ad?.description).toBe(updatedAd.description);
    expect(ad?.category).toBe(updatedAd.category);
  });

  it("should not update ad for unauthorized user", async () => {
    const updatedAd = {
      title: "I'm selling my home",
      description: "it's an old home",
      category: "home",
    };
    await request(app)
      .patch("/ad/" + adId)
      .send(updatedAd)
      .expect(401);
  });

  it("should delete the ad", async () => {
    const user = await User.findById(ownderId);
    expect(user).not.toBeNull();
    if(!user || !user.tokens){
      return
    }
    await request(app)
      .delete("/ad/" + adId)
      .set("Authorization", "Bearer " + user.tokens[0].token)
      .send()
      .expect(200);
    const ad = await Ad.findById(adId);
    expect(ad).toBeNull();
  });
});
