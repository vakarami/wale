import request from "supertest";
import mongoose from "mongoose";
import app from "../app";
import Ad from "../models/ad";
import User from "../models/user";

const userId = new mongoose.Types.ObjectId();
const testUser = {
  name: "test user",
  email: "user111@walle.com",
  password: "pas345sdf",
  _id: userId,
};
beforeAll(async () => {
  await Ad.deleteMany();
  await User.deleteMany();
});

async function getValidToken() {
  const user = await User.findById(userId);
  if (!user || !user.tokens) {
    return;
  }
  return user.tokens[0].token;
}

describe("User basic API", () => {
  it("should post new user", async () => {
    await request(app).post("/user/new").send(testUser).expect(201);
    const user = User.findById(userId);
    expect(user).not.toBeNull();
  });

  it("should get user by id", async () => {
    const res = await request(app).get(`/user/${userId}`);

    expect(res.status).toBe(200);
    expect(res.body._id).toBe(userId.toString());
  });

  it("should get no user by id", async () => {
    const res = await request(app).get("/user/60818799be3c2a1e7beaeca8");

    expect(res.status).toBe(404);
  });

  it("should get no user ads", async () => {
    var res = await request(app).get(`/user/ads/${userId}`);
    expect(res.status).toBe(200);
    expect(res.body.length).toBe(0);
  });

  it("should get user ads", async () => {
    const ad = new Ad({
      title: "I'm selling my ca",
      description: "it's an old car",
      price: 122,
      category: "car",
      owner: userId,
    });
    await ad.save();
    const res = await request(app).get(`/user/ads/${userId}`);
    expect(res.status).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
  });
});

describe("User Auth API", () => {
  it("should be login successfully", async () => {
    const res = await request(app).post("/user/login").send({
      email: testUser.email,
      password: testUser.password,
    });
    expect(res.status).toBe(200);
    const user = await User.findById(userId);
    expect(user).not.toBeNull();
    if (!user || !user.tokens) {
      return;
    }
    expect(user.tokens.map((t) => t.token)).toContain(res.body.token);
  });

  it("should get Profile", async () => {
    const token = await getValidToken();
    const res = await request(app)
      .get("/me")
      .set("Authorization", "Bearer " + token)
      .send();

    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty("name");
    expect(res.body).toHaveProperty("email");
  });

  it("should update Profile", async () => {
    const token = await getValidToken();
    const res = await request(app)
      .patch("/me")
      .set("Authorization", "Bearer " + token)
      .send({
        name: "test user2",
        email: "user111@walle.com",
        password: "pas345sdf",
      });

    expect(res.status).toBe(200);
    const user = await User.findById(userId);
    expect(user?.name).toBe("test user2");
  });

  it("should ask for login first no token", async () => {
    const res = await request(app).get("/me").send();
    expect(res.status).toBe(401);
  });

  it("should ask for login first invalid token", async () => {
    const res = await request(app)
      .get("/me")
      .set(
        "Authorization",
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDgzZTZkZDRjNWZkYTEwYjBkYjU3NDkiLCJpYXQiOjE2MTkyNTk4NTF9.bH2vCGftcH5lsQAj-49fZAV1KzO6EFpHL6EiKG11NUY"
      )
      .send();

    expect(res.status).toBe(401);
  });

  it("should clear current token", async () => {
    const token = await getValidToken();

    const res = await request(app)
      .post("/user/logout")
      .set("Authorization", "Bearer " + token)
      .send();

    const user = await User.findById(userId);
    expect(res.status).toBe(200);
    expect(user?.tokens).toHaveLength(1);
  });

  it("should clear all tokens", async () => {
    const token = await getValidToken();

    const res = await request(app)
      .post("/user/logoutAll")
      .set("Authorization", "Bearer " + token)
      .send();

    const user = await User.findById(userId);
    expect(res.status).toBe(200);
    expect(user?.tokens).toHaveLength(0);
  });
});

describe("User no Auth API", () => {
  it("should login successfully", async () => {
    const res = await request(app).post("/user/login").send({
      email: testUser.email,
      password: testUser.password,
    });
    expect(res.status).toBe(200);
  });

  it("should not allow new user", async () => {
    const token = await getValidToken();
    const res = await request(app)
      .post("/user/new")
      .set("Authorization", "Bearer " + token)
      .send(testUser);

    expect(res.status).toBe(403);
  });

  it("should not allow to login", async () => {
    const token = await getValidToken();

    const res = await request(app)
      .post("/user/login")
      .set("Authorization", "Bearer " + token)
      .send({
        email: testUser.email,
        password: testUser.password,
      });
    expect(res.status).toBe(403);
  });
});
