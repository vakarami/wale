import winston from "winston";
import expressWinston from "express-winston";

const logOptions = {
    transports: [
      new winston.transports.Console()
    ],
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.colorize(),
      winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    meta: false,
    msg: "HTTP  ",
    expressFormat: true,
    colorize: false,
    ignoreRoute: ()=> { return false; }
  };
export const expressLogger = expressWinston.logger(logOptions);
export const logger = winston.createLogger(logOptions);
