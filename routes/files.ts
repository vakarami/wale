import express, {Request, Response, ErrorRequestHandler} from "express";
import multer from "multer";
import sharp from "sharp";
import fs from "fs";
import { logger } from "../log/log";
import { auth } from "../middleware/auth";
var router = express.Router();

const uploadErrorHandler: ErrorRequestHandler = (error, req, res, next) => {
  logger.error(error);
  res.status(400).send({ error: error.message });
}

const photosUpload = multer({
  limits: {
    fileSize: 10000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.toLowerCase().match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error("Please upload an image"));
    }

    cb(null, true);
  },
});

router.post(
  "/photos/upload",
  auth,
  photosUpload.array("photos"),
  async (req: Request, res: Response) => {
    const filepaths = [] as string[];
    (req.files as Express.Multer.File[]).map((file) => {
      const newPath = getNewFilePath();
      sharp(file.buffer).resize({ width: 600, height: 600 }).webp({}).toFile(newPath);
      filepaths.push(newPath);
    });
    res.send(filepaths);
  },
   uploadErrorHandler
);


/**
 *
 * @returns a random file path with this structure: ./photos/yyyy-mm-dd/timestamp.webp
 */
function getNewFilePath() {
  const basePath = "photos/" + new Date().toISOString().slice(0, 10);
  if (!fs.existsSync(basePath)) {
    fs.mkdirSync(basePath);
  }
  const newPath = basePath + "/" + Date.now() + "_" + Math.random() + ".webp";
  return newPath;
}

export default router;
