import express from "express";
import Ad from "../models/ad";
import { auth, UserRequest } from "../middleware/auth";
import { logger } from "../log/log";
import { extractQuery, extractOptions, ReqQuery } from "../utils/query";
import { UserType } from '../models/user';
var router = express.Router();

router.post("/ads/new", auth, async (req: UserRequest, res) => {
  const ad = new Ad(req.body);
  if (!req.user?._id) {
    //never happens, but required for type checking
    return;
  }
  ad.owner = req.user?._id;
  try {
    await ad.save();
    res.status(201).send(ad);
  } catch (error) {
    logger.error("bad post request", error);
    res.status(400).send(error.message);
  }
});

router.get("/ads", async (req, res) => {
  try {
    const adList = await Ad.find(extractQuery(req.query as ReqQuery), null, extractOptions(req.query as ReqQuery));
    res.send(adList);
  } catch (e) {
    logger.error("error in getting ads list", e);
    res.status(400).send(e.message);
  }
});

router.get("/ad/:id", async (req, res) => {
  try {
    const ad = await Ad.findById(req.params.id);
    if (ad == null) {
      return res.status(404).send({});
    }
    await ad.populate("owner").execPopulate();//populate the user details
    if(!ad.owner){ //if user is not available anymore
      return res.status(404).send({});
    }
    
    (ad.owner as UserType).email="";//mask user email
    res.send(ad);
  } catch (e) {
    if (e.message.includes("ObjectId")) {
      return res.status(404).send({});
    }
    logger.error("error in getting ad by id", e);
    res.status(400).send(e.message);
  }
});

router.patch("/ad/:id", auth, async (req: UserRequest, res) => {
  try {
    var ad = await Ad.findById(req.params.id);
    if (ad == null) {
      return res.status(404).send({});
    }
    if (ad.owner.toString() !== req.user?._id.toString()) {
      return res.status(403).send("You are not owner of this Ad");
    }
    const notAllowedKeys = ["owner", "_id"];
    for (const key of notAllowedKeys) {
      delete req.body[key];
    }
    ad = await Ad.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true });
    res.send(ad);
  } catch (e) {
    if (e.message.includes("ObjectId")) {
      return res.status(404).send({});
    }
    logger.error("error in updating ad by id", e);
    res.status(400).send(e.message);
  }
});

router.delete("/ad/:id", auth, async (req: UserRequest, res) => {
  try {
    var ad = await Ad.findById(req.params.id);
    if (ad == null) {
      return res.status(404).send({});
    }
    if (ad.owner.toString() !== req.user?._id.toString()) {
      return res.status(403).send("You are not owner of this Ad");
    }

    ad = await Ad.findByIdAndDelete(req.params.id);
    res.send(ad);
  } catch (e) {
    if (e.message.includes("ObjectId")) {
      return res.status(404).send({});
    }
    logger.error("error in deleting ad by id", e);
    res.status(400).send(e.message);
  }
});

export default router;
