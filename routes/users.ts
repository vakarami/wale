import express from "express";
import User from "../models/user";
import { logger } from "../log/log";
import { auth, noAuth, UserRequest } from "../middleware/auth";
import { extractQuery, extractOptions, ReqQuery } from "../utils/query";
var router = express.Router();

router.get("/user/:id", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (user == null) {
      return res.status(404).send();
    }
    user.email="";//mask user email
    res.send(user);
  } catch (err) {
    logger.error("error in getting user ", err);
    res.status(400).send(err.message);
  }
});

router.get("/user/ads/:id", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (user == null) {
      return res.status(404).send("user not found");
    }
    const match = extractQuery(req.query as ReqQuery);
    await user.populate({ path: "ads", match, options: extractOptions(req.query as ReqQuery) }).execPopulate();
    res.send(user.ads);
  } catch (err) {
    logger.error("error in getting user ads", err);
    res.status(400).send(err.message);
  }
});

router.post("/user/new", noAuth, async (req, res) => {
  try {
    const user = new User(req.body);
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (err) {
    logger.error("error in posting user ", err);
    res.status(404).send(err.message);
  }
});

router.post("/user/login", noAuth, async (req, res) => {
  try {
    const user = await User.findByCredentials(req.body.email, req.body.password);
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (e) {
    res.status(401).send(e.message);
  }
});

router.post("/user/logout", auth, async (req: UserRequest, res) => {
  try {
    if(!req.user){
      return res.send();
    }
    req.user.tokens = req.user.tokens?.filter((token) => {
      return token.token !== req.token;
    });
    await req.user?.save();

    res.send();
  } catch (e) {
    res.status(400).send(e.message);
  }
});

router.post("/user/logoutAll", auth, async (req: UserRequest, res) => {
  try {
    if (req.user) {
      req.user.tokens = [];
      await req.user?.save();
    }
    res.send();
  } catch (e) {
    logger.error("error in deleting tokens", e);
    res.status(400).send(e.message);
  }
});

router.get("/me", auth, async (req: UserRequest, res) => {
  res.send(req.user);
});

router.patch("/me", auth, async (req: UserRequest, res) => {
  try {
    const allowedFields = ["name", "email", "password"];
    const keys = Object.keys(req.body);
    for (const key of keys) {
      if (!allowedFields.includes(key)) {
        delete req.body[key];
      }
    }
    const user = await User.findByIdAndUpdate(req.user?._id, req.body, { new: true, runValidators: true });

    res.send(user);
  } catch (err) {
    logger.error("error in updating user ", err);
    res.status(400).send(err.message);
  }
});

export default router;
