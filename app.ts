import express from "express"
import serveStatic from "serve-static"
import cors from "cors"
import mongoose from "./db/mongoose"

import usersRouter from "./routes/users"
import adsRouter from "./routes/ads"
import filesRouter from "./routes/files"
import  { expressLogger } from "./log/log";

const app = express();

const temp = mongoose;//just to make sure mongoose will be loaded after compile

const corsOptions = {
  origin: process.env.WEB_URL,
};
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
const photosStatic = serveStatic(process.env.PHOTOS_ROOT as string);
app.use("/photos", photosStatic)
app.use(expressLogger);

app.use(usersRouter);
app.use(adsRouter);
app.use(filesRouter);

app.use("*", (req, res) => {
  res.status(404).send("not found");
});

export default app;
