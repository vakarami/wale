FROM node:15.6.0-alpine

WORKDIR /usr/src/app

ENV NODE_ENV production

COPY package*.json ./
RUN npm install --production

COPY . .

RUN npx tsc
COPY package.json ./dist/

ENV PORT 3000
EXPOSE $PORT
ENV PHOTOS_ROOT /usr/src/app/photos
RUN mkdir -p /usr/src/app/photos

CMD [ "npm", "start" ]
