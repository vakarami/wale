import mongoose, { Document, Model, ObjectId } from "mongoose";
import { UserType } from "./user";

export interface AdType extends Document {
  _id?: string;
  title: string;
  description: string;
  price: string | number;
  currency: Currency;
  category: string;
  createdAt: string | Date;
  owner: string | ObjectId | UserType;
  city: string;
  contact: string;
  photos: string[];
}

export enum Currency {
  "USD",
  "SGD",
  "EUR",
}

const AdSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      minlength: 5,
      maxlength: 255,
    },
    description: {
      type: String,
      required: true,
      trim: true,
    },
    category: {
      type: String,
      required: true,
      trim: true,
      lowerCase: true,
      enum: ["car", "house", "mobile", "laptop", "electronics", "home", "wood"],
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    price: {
      type: Number,
      required: true,
      min: 0,
    },
    currency: {
      type: String,
      upperCase: true,
      trim: true,
      enum: Currency,
      default: "USD",
    },
    city: {
      type: String,
      trim: true,
    },
    contact: {
      type: String,
      trim: true,
    },
    photos: [
      {
        type: String,
      },
    ],
  },
  { timestamps: true, strict: true }
);

AdSchema.index({ title: "text", description: "text" }, { name: "Initial text index", weights: { title: 5, description: 1 } });

const Ad: Model<AdType> = mongoose.model("Ad", AdSchema);

export default Ad;
