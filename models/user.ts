import mongoose, { Document, Model, ObjectId, Query } from "mongoose";
import validator from "validator";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { AdType } from "./ad";

export interface UserType extends Document {
  _id: string | ObjectId;
  name: string;
  email: string;
  password?: string;
  tokens?: { _id?: string; token: string }[];
  createdAt?: Date;
  updatedAt?: Date;
  ads?: AdType[];
  generateAuthToken: () => string;
}

export interface UserModel extends Model<UserType>{
  findByCredentials: (email: string, password: string) => UserType;
}

const userSchema = new mongoose.Schema<UserType, UserModel>(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true,
      lowercase: true,
      validate(value: string) {
        if (!validator.isEmail(value)) {
          throw new Error("Email is invalid");
        }
      },
    },
    password: {
      type: String,
      required: true,
      minlength: 7,
      trim: true,
      validate(value: string) {
        if (!/\d/.test(value)) {
          throw new Error("Password must have a digit");
        }
      },
    },
    tokens: [
      {
        token: {
          type: String,
          required: true,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

userSchema.virtual("ads", {
  ref: "Ad",
  localField: "_id",
  foreignField: "owner",
});

userSchema.methods.toJSON = function () {
  const user = this;

  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.tokens;
  delete userObject.__v;
  delete userObject.createdAt;
  delete userObject.updatedAt;

  return userObject;
};

userSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign({ _id: user._id?.toString(), time: Date.now() }, process.env.JWT_SECRET as string);

  user.tokens = user.tokens?.concat({ token });
  await user.save();

  return token;
};

userSchema.pre("save", async function (next) {
  const user = this;

  if (user.isModified("password") && user.password) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  next();
});
/**
 * hashing password before update
 */
userSchema.pre<Query<UserType, UserType>>("findOneAndUpdate", async function (next) {
  try {
    const _update = this.getUpdate() as UserType;

    if (_update.password) {
      const hashed = await bcrypt.hash(_update.password, 8);
      _update.password = hashed;
    }
    next(null);
  } catch (err) {
    return next(err);
  }
});

userSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error("Unable to login");
  }

  const isMatch = await bcrypt.compare(password, user.password as string);

  if (!isMatch) {
    throw new Error("Unable to login");
  }

  return user;
};

const User = mongoose.model("User", userSchema);

export default User;
