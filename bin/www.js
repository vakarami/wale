import http from "http";
import app from "../app";
import { logger } from "../log/log";

const port = process.env.PORT || 3000;
app.set("port", port);

const server = http.createServer(app);

server.listen(port);
server.on("error", (e) => logger.error("error in starting server", e));
server.on("listening", () => logger.info("server started at " + port));
